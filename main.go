package main

import (
	"fabric-sdk-go-test/utils"
	"fmt"
)

func main() {

	setup := utils.FabricSetup{
		OrdererID: "orderer.wnzx.com",
		ChannelID: "mychannel",
		ChannelConfig: "/home/fangdengfu/disk/golang/gopath/src/fabric-sdk-go-test/maiyi/e2e/channel/channel.tx",

		ChaincodeID: "tokenAccount",
		ChaincodeVersion: "",
		ChaincodeGoPath: "/home/fangdengfu/disk/golang/gopath/src/fabric-sdk-go-test/maiyi/e2e/gocc/",
		ChaincodePath: "github.com/tokens/account/",

		OrgAdmin: "Admin",
		OrgName: "Org1",
		ConfigFile: "config.yaml",

		UserName: "User1",

		DefaultPeer: "peer0.org1.wnzx.com",
	}

	setup.Initialize()

	defer setup.CloseSDK()

	fcn := "getAccount"
	res, err := setup.Query(setup.ChaincodeID, fcn, []string{"033505a0c8f3bef1325574a1862ea1ea1c326d69"})
	if err != nil {
		fmt.Printf("query %s.%s error: %v\n", setup.ChaincodeID, fcn, err)
		return
	}
	fmt.Println(res)

}
