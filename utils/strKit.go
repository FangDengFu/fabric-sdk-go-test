package utils

import "unsafe"

func Str2Bytes(s string) []byte {
	x := (*[2]uintptr)(unsafe.Pointer(&s))
	h := [3]uintptr{x[0], x[1], x[1]}
	return *(*[]byte)(unsafe.Pointer(&h))
}

func BatchStr2Bytes(s []string) [][]byte {
	var res [][]byte
	if len(s) == 0 {
		return res
	}
	for _, k := range s {
		res = append(res, Str2Bytes(k))
	}
	return res
}
